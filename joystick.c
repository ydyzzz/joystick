#include <fcntl.h>
#include <linux/joystick.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

enum command {
    Nope = 0,
    MoveUp,
    MoveDown,
    MoveLeft,
    MoveRight,
    ClickLeftPress,
    ClickLeftRelease,
    ClickRightPress,
    ClickRightRelease,
    IncrSens,
    DecrSens
};

const float sens_step = .5f;
const float default_sens = 25.f;

struct joystick {
    int fd;
    struct js_event event;
    uint16_t x;
    uint16_t y;
    float sens;
    uint16_t cmd;
    Display *display;
    Window *root_window;
};

void x_mouse_click(Display *display, int button, int type) {
    XEvent event;
    memset(&event, 0, sizeof(event));
    event.xbutton.button = button;
    event.xbutton.same_screen = True;
    event.xbutton.subwindow = DefaultRootWindow(display);
    while (event.xbutton.subwindow) {
        event.xbutton.window = event.xbutton.subwindow;
        XQueryPointer(display, event.xbutton.window,
                      &event.xbutton.root, &event.xbutton.subwindow,
                      &event.xbutton.x_root, &event.xbutton.y_root,
                      &event.xbutton.x, &event.xbutton.y,
                      &event.xbutton.state);
    }
    event.type = type;
    if (XSendEvent(display, PointerWindow, True, ButtonPressMask, &event) == 0) {
        fprintf(stderr, "Error to send the event!\n");
    }
    XFlush(display);
}

void joystick_init(struct joystick *js, int fd, Display *display, Window *root_window) {
    js->fd = fd;
    js->x = 0;
    js->y = 0;
    js->sens = default_sens;
    js->cmd = 0x0001;
    js->display = display;
    js->root_window = root_window;
}

void joystick_set_command(struct joystick *js, enum command cmd) {
    js->cmd |= 1 << cmd;
}

 void joystick_unset_command(struct joystick *js, enum command cmd) {
    js->cmd &= ~(1 << cmd);
}

 int joystick_isset_command(struct joystick *js, enum command cmd) {
    return (js->cmd >> cmd) & 1;
}

void joystick_handle(struct joystick *js, void (*event_callback)(struct joystick*), void (*cmd_callback)(struct joystick*)) {
    while (read(js->fd, &(js->event), sizeof(struct js_event)) > 0) {
        event_callback(js);
    }
    cmd_callback(js);
}

 void joystick_click_press(struct joystick *js, int button) {
     x_mouse_click(js->display, button, ButtonPress);
}

 void joystick_click_release(struct joystick *js, int button) {
    x_mouse_click(js->display, button, ButtonRelease);
}

 void joystick_scroll_up(struct joystick *js) {
// TODO: Implement
}

 void joystick_scroll_down(struct joystick *js) {
// TODO: Implement
}

void joystick_free(struct joystick *js) {
    close(js->fd);
    XCloseDisplay(js->display);
}


void joystick_event_handler(struct joystick *js) {
    if (js->event.type != JS_EVENT_BUTTON) {
        return;
    }

    if (js->event.number == 0) {
        js->event.value == 1 ? 
            joystick_set_command(js, MoveDown) :
            joystick_unset_command(js, MoveDown);
    }
    if (js->event.number == 1) {
        js->event.value == 1 ? 
            joystick_set_command(js, MoveRight) :
            joystick_unset_command(js, MoveRight);
    }
    if (js->event.number == 2) {
        js->event.value == 1 ? 
            joystick_set_command(js, MoveLeft) :
            joystick_unset_command(js, MoveLeft);
    }
    if (js->event.number == 3) {
        js->event.value == 1 ? 
            joystick_set_command(js, MoveUp) :
            joystick_unset_command(js, MoveUp);
    }

    if (js->event.number == 4) {
        js->event.value == 1?
            joystick_set_command(js, ClickLeftPress) :
            joystick_set_command(js, ClickLeftRelease);
    }

    if (js->event.number == 5) {
        js->event.value == 1?
            joystick_set_command(js, ClickRightPress) :
            joystick_set_command(js, ClickRightRelease);
    }

    if (js->event.number == 6) {
        js->event.value == 1?
            joystick_set_command(js, IncrSens) :
            joystick_unset_command(js, IncrSens);
    }

    if (js->event.number == 7) {
        js->event.value == 1?
            joystick_set_command(js, DecrSens) :
            joystick_unset_command(js, DecrSens);
    }
}

void joystick_cmd_handler(struct joystick *js) {
    if (joystick_isset_command(js, MoveUp)) {
        js->y -= (uint16_t)js->sens;
        printf("↓\n");
    }

    if (joystick_isset_command(js, MoveDown)) {
        js->y += (uint16_t)js->sens;
        printf("↑\n");
    }

    if (joystick_isset_command(js, MoveLeft)) {
        js->x -= (uint16_t)js->sens;
        printf("←\n");
    }

    if (joystick_isset_command(js, MoveRight)) {
        js->x += (uint16_t)js->sens;
        printf("→\n");
    }

    if (joystick_isset_command(js, ClickLeftPress)) {
        joystick_click_press(js, Button1);
        joystick_unset_command(js, ClickLeftPress);
        printf("Click Left Press\n");
    }

    if (joystick_isset_command(js, ClickLeftRelease)) {
        joystick_click_release(js, Button1);
        joystick_unset_command(js, ClickLeftRelease);
        printf("Click Left Release\n");
    }

    if (joystick_isset_command(js, ClickRightPress)) {
        joystick_click_press(js, Button3);
        joystick_unset_command(js, ClickRightPress);
        printf("Click Right Press\n");
    }

    if (joystick_isset_command(js, ClickRightRelease)) {
        joystick_click_release(js, Button3);
        joystick_unset_command(js, ClickRightRelease);
        printf("Click Right Release\n");
    }

    if (joystick_isset_command(js, IncrSens)) {
        js->sens += sens_step;
        printf("Increase Sens: %f\n", js->sens);
    }

    if (joystick_isset_command(js, DecrSens)) {
        js->sens -= sens_step;
        printf("Descrease Sens: %f\n", js->sens);
    }

    XWarpPointer(js->display, None, *(js->root_window), 0, 0, 0, 0, js->x, js->y);
    XFlush(js->display);
}

int main(void) {
    Display *display;
    Window root_window;
    display = XOpenDisplay(0);
    root_window = XRootWindow(display, 0);
    XSelectInput(display, root_window, KeyReleaseMask);

    XWarpPointer(display, None, root_window, 0, 0, 0, 0, 0, 0);
    XFlush(display);

    int js_fd = open("/dev/input/js0", O_RDONLY);
    if (js_fd == -1) {
        perror("/dev/input/js0");
        return -1;
    }
    if (fcntl(js_fd, F_SETFL, O_NONBLOCK) == -1) {
        perror("nonblocking read");
        return -1;
    }

    struct joystick js;
    joystick_init(&js, js_fd, display, &root_window);

    while (1) {
        joystick_handle(&js, joystick_event_handler, joystick_cmd_handler);
        usleep(50 * 1000);
    }

    joystick_free(&js);
    return 0;
}
