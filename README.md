# Joystick
Joystick is a very pet project that allows you to control a mouse via gamepad.
## Building
```bash
mkdir build
cd build
cmake ..
cmake --build .
```
## Usage
```bash
./joystick
```
Use face buttons to control a mouse position